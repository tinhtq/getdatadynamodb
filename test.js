var AWS = require("aws-sdk");
const fs = require('fs');
const atob = require('atob');
const path = require('path');
const { log } = require("console");
require('dotenv').config()
AWS.config.update({
    region: process.env.REGION,
    endpoint: process.env.ENTPOINT,
    "accessKeyId": process.env.ACCESS_KEY_ID,
    "secretAccessKey": process.env.SECRET_ACCESS_KEY
});

var docClient = new AWS.DynamoDB.DocumentClient();

var params = {
    TableName: process.env.TABLE_NAME,
    ProjectionExpression: "#timestamp,payload",
    ExpressionAttributeNames: {
        "#timestamp": "timestamp",
    },
};

console.log("Scanning Movies table.");
docClient.scan(params, onScan);

function onScan(err, data) {
    if (err) {
        console.error("Unable to scan the table. Error JSON:", JSON.stringify(err, null, 2));
    } else {
        console.log("Scan succeeded.");
        let arr = []
        data.Items.forEach(function (data) {
            let x = base64ToHex(data.payload.data).toLowerCase();
            let datetime = new Date(data.payload.ts)
            arr.push({ data: x, ts:datetime, device: process.env.TABLE_NAME })
        });
        arr.sort((a, b) => (a.ts < b.ts) ? 1 : ((b.ts < a.ts) ? -1 : 0))
        fs.writeFileSync(path.resolve(__dirname, 'data.json'), JSON.stringify(arr));
        if (typeof data.LastEvaluatedKey != "undefined") {
            console.log("Scanning for more...");
            params.ExclusiveStartKey = data.LastEvaluatedKey;
            docClient.scan(params, onScan);
        }
        console.log(arr.length);
    }
}
function base64ToHex(str) {
    const raw = atob(str);
    let result = '';
    for (let i = 0; i < raw.length; i++) {
        const hex = raw.charCodeAt(i).toString(16);
        result += (hex.length === 2 ? hex : '0' + hex);
    }
    return result.toUpperCase();
}